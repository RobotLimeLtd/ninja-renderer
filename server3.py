import argparse
import os
import logging

from flask import Flask, flash, redirect, render_template, request, session, abort

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--debug", help="run in debug mode",
                    action="store_true")
parser.add_argument("--app-name", help="set application hame")
args = parser.parse_args()

app_name = os.getenv("APP_NAME", 'jinja-renderer')
app = Flask(__name__)

@app.route("/")
def hello():
  return render_template('home.html', app_name=app_name)

if __name__ == '__main__':
    port = int(os.getenv("APP_PORT", 5000))
    host = os.getenv("APP_HOST", '0.0.0.0')
    app.run(host=host, port=port, debug=args.debug, threaded=True)
