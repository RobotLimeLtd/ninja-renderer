FROM python:3.7-alpine
RUN mkdir /project
WORKDIR /project
ADD . .
RUN find . -name '*.pyc' -delete
RUN pip install --no-cache-dir -r requirements.txt
CMD [ "python", "server3.py" ]
EXPOSE 5000/tcp
