import logging
import yaml

from jinja2 import Template

def from_yaml(data, template):
  yaml_data = yaml.load(data)
  jinja_template = Template(template)
  return jinja_template.render(yaml_data)