# JinjaR

A simple Jinja 2 Renders

## Run the app

### In Docker (preferred)

```
docker image build -t jinjar .
docker container run -p 5000:5000 jinjar
```

### Old fashioned (requires virtualenv)

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
python ./server3.py
```

## Running Tests

### In Docker (preferred)

```
docker container run jinjar "pytest"
```


### Old fashioned (requires virtualenv)
```
pytest
```
